FROM python:3.6-slim
MAINTAINER Rod Mooseman
WORKDIR /app
ADD . /app

ENV NAME World
CMD ["python", "app.py"]
